<?php

namespace Drupal\search_api_tax_filter\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\QueryInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes entities marked as 'excluded' from being indexes.
 *
 * @SearchApiProcessor(
 *   id = "search_api_tax_filter_processor",
 *   label = @Translation("Taxonomy Filter"),
 *   description = @Translation("Filter items to be indexed by taxonomy term."),
 *   stages = {
 *     "alter_items" = -50
 *   }
 * )
 */
class SearchApiTaxFilterProcessor extends ProcessorPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  protected $entityTypeManager;


  /**
   * @var $account \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityManager, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $vids = Vocabulary::loadMultiple();

    foreach ($vids as $vid => $vobj) {
          $options = array();
          $enabled = ($this->configuration['fields'][$vid][0] == 1 ? TRUE : FALSE);
          $form['fields'][$vid]['vocab'] = [
            '#type' => 'checkbox',
            '#title' => $vobj->label(),
            '#default_value' => $enabled,
          ];
          $terms = $this->entityManager->getStorage('taxonomy_term')->loadTree($vobj->id());
          if (!empty($terms)) {
            foreach ($terms as $term) {
              $indent = '';
              $i = 1;
              while ($term->depth >= $i) {
                $indent .= '-';
                $i++;
              }
              $options[$term->tid] = $indent . ' ' . $term->name;
            }
          }

          $form['fields'][$vid]['terms'] = [
            '#type' => 'checkboxes',
            '#title' => $this->t('Select Terms'),
            '#description' => $this->t("Choose terms to refine content to be indexed"),
            '#options' => $options,
            '#default_value' => $enabled ? $this->configuration['fields'][$vid][1] : key($options),
            '#access' => count($options) > 1,
            '#states' => [
              'visible' => [
                ":input[name=\"processors[search_api_tax_filter_processor][settings][fields][$vid][vocab]\"]" => [
                  'checked' => TRUE,
                ],
              ],
            ],
          ];
    }
        return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Remove non selected values.
    if (isset($values['fields']) && is_array($values['fields'])) {
      foreach ($values['fields'] as $entity => $field) {
        $values['fields'][$entity] = array_values(array_filter($field));
      }
    }

    $this->setConfiguration($values);
  }



  /**
   * @inheritDoc
   */
  public function alterIndexedItems(array &$items) {
    $config = $this->getConfiguration()['fields'];
    $termIds = $this->flattenTermsIds($config);

    $query = \Drupal::database()->select('taxonomy_index', 'ti');
    $query->fields('ti', array('nid'));
    $query->condition('ti.tid', $termIds, 'IN');
    $query->distinct(TRUE);
    $result = $query->execute();
    $nodeIds = $result->fetchCol();
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      $entity_id = $object->get('nid')->getString();
      $search_result = array_search($entity_id, $nodeIds, FALSE);
      if ($search_result === FALSE) {
        unset($items[$item_id]);
        continue;
      }
    }
  }

  /**
   * @param $config
   *
   * @return array
   */
  private function flattenTermsIds($config) {
    $termIds = [];
    foreach ($config as $vocab) {
      if (isset($vocab[0]) && $vocab[0] == 1) {
        foreach ($vocab[1] as $term) {
          if ($term != 0) {
            $termIds[] = $term;
          }
        }
      }
    }
   return $termIds;
  }

}
